import msgpack
import cv2
import os
import pika
import datetime


def callback(ch, method, properties, body):
    print(" [x] Received messege")
    print('FILE RECIVED - ', datetime.datetime.now())

connection = pika.BlockingConnection(pika.ConnectionParameters('rabbitmq', 5672, '/', pika.PlainCredentials('user', 'kwK6jQnqAH')))
channel = connection.channel()
channel.queue_declare(queue='PPS1')

channel.basic_consume(queue='PPS1', on_message_callback=callback, auto_ack=True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()